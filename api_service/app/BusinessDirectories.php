<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessDirectories extends Model
{
    //
    protected $fillable = ['code','name','active'];
    
    public function placeTypes()
    {
        return $this->hasMany('App\BusinessDirectoryPlaceTypes', 'business_id');
        
    }
}
