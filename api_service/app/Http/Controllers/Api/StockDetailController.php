<?php
/**
 * Created by PhpStorm.
 * User: JonnyNguyen
 * Date: 10/08/2016
 * Time: 09:56
 */

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Helpers\Response;
use App\Helpers\LeadHelper;
use App\Http\Requests;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use App\Services\FactoryService;

class StockDetailController extends ApiController
{
    public function __construct()
    {
        $this->service = FactoryService::getStockDetailService();
    }

    public function index()
    {
        $stockDetailFilter = Input::get();
        $stockDetailQueryBuilder = $this->service->getStockDetailListingQueryBuilder($stockDetailFilter);

        $stockDetailFilter['perPage'] = isset($stockDetailFilter['perPage']) ? $stockDetailFilter['perPage'] : 10;
        $stockDetailFilter['page'] = isset($stockDetailFilter['page']) ? $stockDetailFilter['page'] : 1;

        $stockDetailPaginator = $stockDetailQueryBuilder->paginate($stockDetailFilter['perPage'], ['*'], 'page', $stockDetailFilter['page']);

        $pageCount = $stockDetailPaginator->lastPage();

        $leads = $this->service->getStockDetailFromQueryBuilder($stockDetailQueryBuilder);

        return Response::responseWithPageCount($leads, 200, 'OK', [], $pageCount);
    }

    public function show(Request $request, $stockDetailId)
    {
        $stockDetail = $this->service->findResource($stockDetailId);

        if ($stockDetail)
            return Response::response($this->service->transform($stockDetail));

        return Response::responseNotFound();
    }

    public function store(Request $request)
    {
        $stockDetailInfo = $request->get('stock_details');

        $data = [];
        $errorMsg = [];

        if (!empty($stockDetailInfo))
        {
            foreach ($stockDetailInfo as $info)
            {
                //validate
                $validator = $this->service->validateInfo($info);
                if ($validator->fails())
                {
                    $errors = $validator->errors()->all();
                    $errorMsg = array_merge($errorMsg, $errors);
                } else
                    $data[] = $this->service->insert($info);
            }
        } else
        {
            Response::response($data, 400, 'Data is empty.');
        }

        if (!empty($errorMsg))
        {
            return Response::responseValidateFailed(implode(' | ', $errorMsg), $data);
        }
        return Response::response($data);
    }

    public function update(Request $request, $id)
    {
        $info = $request->all();
        $stockDetail = $this->service->findResource($id);

        if (!$stockDetail)
            return Response::responseNotFound();

        //validate
        $validator = $this->service->validateInfo($info, 'update', $id);
        if ($validator->fails())
        {
            $errorMsg = $validator->errors()->all();
            return Response::responseValidateFailed(implode(' | ', $errorMsg));
        }
        //update info
        $data = $this->service->update($stockDetail, $info);
        return Response::response($data);
    }

    public function destroy($stockDetailId){
        $isStockDetailDeleted = $this->service->delete($stockDetailId);
        if ($isStockDetailDeleted)
            return Response::response([]);
        return Response::responseNotFound();
    }
}