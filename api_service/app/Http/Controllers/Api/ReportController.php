<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Helpers\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Services\FactoryService;


class ReportController extends Controller
{

    public function __construct()
    {
        $this->service = FactoryService::getReportLeadService();
    }

    public function getLeadTypeAnalysis()
    {
        $leadParams = Input::get();
        if (isset($leadParams['year']) && trim($leadParams['year'])) {
            $year = $leadParams['year'];
            $data = $this->service->transLeadType($year);
            return Response::response($data);
        }
    }

    public function getLeadAllocate()
    {
        $leadParams = Input::get();

        if (isset($leadParams['year']) && trim($leadParams['year'])) {
            $year = $leadParams['year'];
            $month = $leadParams['month'];
            $data = $this->service->transLeadAllocate($month, $year);
            return Response::response($data);
        }
    }
    public function getLeadSource()
    {
        $leadParams = Input::get();

        if (isset($leadParams['year']) && trim($leadParams['year'])) {
            $year = $leadParams['year'];
            $month = $leadParams['month'];
            $data = $this->service->transLeadSource($month, $year);
            return Response::response($data);
        }
    }
}
