<?php
/**
 * Created by PhpStorm.
 * User: JonnyNguyen
 * Date: 02/08/2016
 * Time: 11:03
 */

namespace App\Http\Controllers\Api;

use App\Lead;
use App\Task;
use App\Remark;
use Illuminate\Http\Request;
use App\Helpers\Response;
use App\Services\FactoryService;
use Illuminate\Support\Facades\Input;
use App\Helpers\StringHelper;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Helpers\LeadHelper;
use App\Organization;
use App\Contact;

use App\Http\Requests;

class CustomerController extends ApiController
{
    protected $service_lead;
    protected $service_task;
    protected $service_attendance;
    protected $service_remark;

    public function __construct()
    {
        $this->service = FactoryService::getCustomerService();
        $this->service_lead = FactoryService::getLeadService();
        $this->service_task=FactoryService::getTaskService();
        $this->service_attendance= FactoryService::getAttendanceService();
        $this->service_remark =FactoryService::getRemarkService();
    }

    public function index()
    {
        $customerFilter = Input::get();
        $customerQueryBuilder = $this->service->getCustomerListingQueryBuilder($customerFilter);

        $customerFilter['perPage'] = isset($customerFilter['perPage']) ? $customerFilter['perPage'] : 10;
        $customerFilter['page'] = isset($customerFilter['page']) ? $customerFilter['page'] : 1;

        $customerPaginator = $customerQueryBuilder->paginate($customerFilter['perPage'], ['*'], 'page', $customerFilter['page']);

        $pageCount = $customerPaginator->lastPage();

        $users = $this->service->getCustomersFromQueryBuilder($customerQueryBuilder);

        return Response::responseWithPageCount($users, 200, 'OK', [], $pageCount);
    }

    public function show(Request $request, $id)
    {
        $customer = $this->service->findResource($id);

        if ($customer)
            return Response::response($this->service->transform($customer));

        return Response::responseNotFound();
    }

    public function store(Request $request)
    {
        $customersInfo = $request->get('customers');
        $data = [];
        $data_lead=[];
        $data_task=[];
        $data_attendance=[];
        $data_remark=[];
        $errorMsg = [];
        
        if (!empty($customersInfo)) {
            foreach ($customersInfo as $info) {
                $validator = $this->service->validateInfo($info);
                if ($validator->fails()) {
                    $errors = $validator->errors()->all();
                    $errorMsg = array_merge($errorMsg, $errors);
                } else
                    $data_lead[]= $this->service_lead->insert($this->service->transform_customer_to_lead($info));
                    $data_task[]=$this->service_task->insert($this->service->transform_customer_to_task($info));
                    $data_attendance[]=$this->service_attendance->insert($this->service->transform_customer_to_attendance($info));
                    $data_remark[]=$this->service_remark->insert($this->service->transform_customer_to_remark($info));
            }
        } else {
            Response::response($data, 400, 'Data is empty.');
        }

        if (!empty($errorMsg)) {
            return Response::responseValidateFailed(implode(' | ', $errorMsg), $data);
        }
        return Response::response($data);
    }
    
}
