<?php
/**
 * Created by PhpStorm.
 * User: JonnyNguyen
 * Date: 02/08/2016
 * Time: 11:03
 */

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Helpers\Response;
use App\Services\FactoryService; 
use Illuminate\Support\Facades\Input;
use App\Helpers\StringHelper;
use App\Http\Requests;


class ContactController extends ApiController
{
    public function __construct()
    {
        $this->service = FactoryService::getContactService();
    }

    public function index()
     {
        $contactFilter = Input::get();
        $contactQueryBuilder = $this->service->getContactListingQueryBuilder($contactFilter);

        $contactFilter['perPage'] = isset($contactFilter['perPage']) ? $contactFilter['perPage'] : 10;
        $contactFilter['page'] = isset($contactFilter['page']) ? $contactFilter['page'] : 1;
        $contactPaginator = $contactQueryBuilder->paginate($contactFilter['perPage'], ['*'], 'page', $contactFilter['page']);
        $pageCount = $contactPaginator->lastPage();
        $users = $this->service->getContactsFromQueryBuilder($contactQueryBuilder);

        return Response::responseWithPageCount($users, 200, 'OK', [], $pageCount);
     }

    public function show(Request $request, $id)
    {
        $contact = $this->service->findResource($id);

        if ($contact)
            return Response::response($this->service->transform($contact));

        return Response::responseNotFound();
    }

    public function store(Request $request) {
        $newContacts = $request->get('contacts');

        $data = [];
        $errorMsg = [];

        if (empty($newContacts)) {
            return Response::response($data, 400, 'Data is empty.');
        }

        foreach ($newContacts as $newContact) {
            $validator = $this->service->validateInfo($newContact);
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                $errorMsg = array_merge($errorMsg, $errors);
                return Response::responseValidateFailed(implode(' | ', $errorMsg));
            }

            $data = $this->service->insert($newContact);
        }

        if (!empty($errorMsg)) {
            return Response::responseValidateFailed(implode(' | ', $errorMsg), $data);
        }

        return Response::response($data);
   }

    public function update(Request $request, $id)
    {
        $info = $request->all();
        $lead = $this->service->findResource($id);

        if (!$lead)
            return Response::responseNotFound();

        //validate
        $validator = $this->service->validateInfo($info, 'update', $id);
        if ($validator->fails()) {
            $errorMsg = $validator->errors()->all();
            return Response::responseValidateFailed(implode(' | ', $errorMsg));
        }
        //update info
        $data = $this->service->update($lead, $info);
        return Response::response($data);
    }
   
  public function destroy($contactId) {

      $iscontactDeleted = $this->service->delete($contactId);

      if ($iscontactDeleted)
          return Response::response([]);
      return Response::responseNotFound();
     }
    
 }
 
