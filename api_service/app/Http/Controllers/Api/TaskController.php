<?php

namespace App\Http\Controllers\Api;

use App\Organization;
use App\Contact;
use App\Services\FactoryService;
use App\Task;
use Illuminate\Support\Facades\Input;
use App\Helpers\Response;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Lead;
use App\Role;
use App\Opportune;
use Tymon\JWTAuth\Facades\JWTAuth;

class TaskController extends ApiController
{
    public function __construct()
    {
        $this->service = FactoryService::getTaskService();
    }
    
    public function index()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::authenticate($token);

        if (!in_array($user->role->id, [Role::ROLE_ADMIN, Role::ROLE_SYS_ADMIN]))
            Input::merge(array('assigned_to' => $user->id));

        $taskFilter = Input::get();
        $taskQueryBuilder = $this->service->getTaskListingQueryBuilder($taskFilter);

        $taskFilter['perPage'] = isset($taskFilter['perPage']) ? $taskFilter['perPage'] : 10;
        $taskFilter['page'] = isset($taskFilter['page']) ? $taskFilter['page'] : 1;

        $taskPaginator = $taskQueryBuilder->paginate($taskFilter['perPage'], ['*'], 'page', $taskFilter['page']);

        $pageCount = $taskPaginator->lastPage();

        $tasks = $this->service->getTasksFromQueryBuilder($taskQueryBuilder);

        return Response::responseWithPageCount($tasks, 200, 'OK', [], $pageCount);
    }
    public function indexWeb()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::authenticate($token);
       
        $taskQueryBuilder = $this->service->getTaskListingQueryBuilderForWeb();

        return Response::response($taskQueryBuilder);
        //return Response::responseWithPageCount($tasks, 200, 'OK', [], $pageCount);
    }
    public function show(Request $request, $taskId)
    {
        $task = $this->service->findResource($taskId);

        if ($task)
            return Response::response($this->service->transform($task));

        return Response::responseNotFound();
    }
    public function store(Request $request)
    {
        $tasksInfo = $request->get('tasks');

        $data = [];
        $errorMsg = [];

        if (empty($tasksInfo)) {
            return Response::response($data, 400, 'Data is empty.');
        }

        foreach($tasksInfo as $taskInfo) {
            $validator = $this->service->validateInfo($taskInfo);
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                $errorMsg = array_merge($errorMsg, $errors);
                return Response::responseValidateFailed(implode(' | ', $errorMsg));
            }

            if (!isset($taskInfo['assigned_to']) || !isset($taskInfo['assigned_to']['id']) || !User::find($taskInfo['assigned_to']['id'])) {
                return Response::responseValidateFailed("The assignee is not existed.");
            }

            if (isset($taskInfo['lead']) && isset($taskInfo['lead']['id']) && !Lead::find($taskInfo['lead']['id'])) {
                return Response::responseValidateFailed("The lead is not existed.");
            }
         
            $assignedLead = Lead::find($taskInfo['lead']['id']);
            $assignedLead->status = Lead::STATUS_ASSIGNED;
            $assignedLead->save();

            if (isset($taskInfo['opportune']) && isset($taskInfo['opportune']['id']) && !Opportune::find($taskInfo['opportune']['id'])) {
                return Response::responseValidateFailed("The opportune is not existed.");
            }

            if (isset($taskInfo['organization']) && isset($taskInfo['organization']['id']) && !Organization::find($taskInfo['organization']['id'])) {
                return Response::responseValidateFailed("The organization is not existed.");
            }
            if (isset($taskInfo['contact']) && isset($taskInfo['contact']['id'])&& !Contact::find($taskInfo['contact']['id']))
            {
                return Response::responseValidateFailed("The contact is not existed.");
            }

            $data = $this->service->insert($taskInfo);
        }

        if (!empty($errorMsg))
        {
            return Response::responseValidateFailed(implode(' | ', $errorMsg), $data);
        }

        return Response::response($data);
    }
    public function update(Request $request, $id)
    {
        $info = $request->all();
        $lead = $this->service->findResource($id);

        if (!$lead)
            return Response::responseNotFound();

        //validate
        $validator = $this->service->validateInfo($info, 'update', $id);
        if ($validator->fails()) {
            $errorMsg = $validator->errors()->all();
            return Response::responseValidateFailed(implode(' | ', $errorMsg));
        }

        if (isset($info['assigned_to']) && isset($info['assigned_to']['id']) && !User::find($info['assigned_to']['id'])) {
            return Response::responseValidateFailed("The assignee is not existed.");
        }

        if (isset($info['lead']) && isset($info['lead']['id']) && !Lead::find($info['lead']['id'])) {
            return Response::responseValidateFailed("The lead is not existed.");
        }

        if (isset($taskInfo['opportune']) && isset($taskInfo['opportune']['id']) && !Opportune::find($taskInfo['opportune']['id'])) {
            return Response::responseValidateFailed("The opportune is not existed.");
        }

        if (isset($taskInfo['organization']) && isset($taskInfo['organization']['id']) && !Lead::find($taskInfo['organization']['id'])) {
            return Response::responseValidateFailed("The organization is not existed.");
        }

        //update info
        $data = $this->service->update($lead, $info);
        return Response::response($data);
    }
    public function destroy($taskId){
        $isTaskDeleted = $this->service->delete($taskId);

        if ($isTaskDeleted)
            return Response::response([]);
        return Response::responseNotFound();
    }
}
