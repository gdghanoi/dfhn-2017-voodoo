<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Response;

class TimeController extends Controller
{
    public function getCurrentServerTime()
    {
        return Response::response([
            "time" => date('Y-m-d H:i:s')
        ]);
    }
}
