<?php
namespace App\Http\Controllers\Api;

use App\BusinessOrder;
use App\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\FactoryService;
use App\Helpers\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Input;

class CustOrganizationController extends ApiController
{
    protected $talentService;

    public function __construct()
    {
        $this->service = FactoryService::getCustOrganizationService();
        $this->talentService = FactoryService::getTalentService();
    }

    public function showByCode(Request $request, $code)
    {
        $organization = $this->service->getCustOrganizationByCode($code);

        if ($organization)
            return Response::response($this->service->transform($organization));

        return Response::responseNotFound();
    }

    public function store(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::authenticate($token);
        $userProfileBase = $this->talentService->getUserProfileBase($user->id);

        if (!empty($userProfileBase))
            return Response::responseForbidden('You belong to an organization already!');

        $newCustOrganization = [];
        $errorMsg = [];
        $errors = [];

        $info = $request->all();
        $info = $this->verifyStoreRequest($info);

        if (isset($info['parent_id']) && $info['parent_id'] != 0) // parent_id from parent_code
        {
            $isParentOrganizationExists = $this->checkOrganizationExists($info['parent_id']);
            if (!$isParentOrganizationExists)
                return Response::responseNotFound('Invalid Parent Organization');
        }

        if (!empty($info)) {
            $validator = $this->service->validateInfo($info);
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                $errorMsg = array_merge($errorMsg, $errors);
            } else {
                $newCustOrganization = $this->insertNewUserBusinessOrganization($info);
                $this->insertDefaultBusinessOrder($newCustOrganization['id']);

                $orgFirstEmployee = $this->insertNewEmployeeForNewOrganization($newCustOrganization['id'], $user);
                $this->linkUserToEmployee($orgFirstEmployee, $user);
                $this->updateUserRoleAdmin($user);
            }
        } else {
            Response::response($newCustOrganization, 400, 'Data is empty.');
        }

        if (!empty($errorMsg)) {
            return Response::responseValidateFailed(implode(' | ', $errorMsg), $newCustOrganization);
        }

        return Response::response($newCustOrganization);
    }

    public function joinExistingOrganization(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::authenticate($token);
        $userProfileBase = $this->talentService->getUserProfileBase($user->id);

        if (!empty($userProfileBase))
            return Response::responseForbidden('You belong to an organization already!');

        $requestBody = Input::get();

        if (empty($requestBody['org_code']))
            return Response::responseMissingParameter("org_code parameter missing!");

        $organizationCode = $requestBody['org_code'];
        $organization = $this->service->getCustOrganizationByCode($organizationCode);

        if (!$organization)
            return Response::responseNotFound("Invalid Organization Code!");

        $newEmployee = $this->insertNewEmployeeForNewOrganization($organization->id, $user);
        $this->linkUserToEmployee($newEmployee, $user);

        return Response::response([]);
    }

    public function verifyStoreRequest($info)
    {
        $info['parent_id'] = 0; // prevent injection
        $info['active'] = true;

        if (isset($info['parent_code'])) {
            $parentCode = $info['parent_code'];
            $parentOrganization = $this->service->getCustOrganizationByCode($parentCode);

            if ($parentOrganization)
                $info['parent_id'] = $parentOrganization->id;
        }

        return $info;
    }

    public function checkOrganizationExists($organizationId)
    {
        $organization = $this->service->findResource($organizationId);
        return $organization ? true : false;
    }

    public function insertNewUserBusinessOrganization($custOrganizationInfo)
    {
        $custOrganizationInfo['active'] = true;
        return $this->service->insert($custOrganizationInfo);
    }

    public function insertDefaultBusinessOrder($organizationId)
    {
        $businessOrderService = FactoryService::getBusinessOrderService();
        $businessOrder = [];
        $businessOrder['plan_id'] = BusinessOrder::DEFAULT_PLAN_ID;
        $businessOrder['org_id'] = $organizationId;
        $businessOrder['status'] = BusinessOrder::STATUS_ACTIVE;

        return $businessOrderService->insert($businessOrder);
    }

    public function insertNewEmployeeForNewOrganization($organizationId, $user)
    {
        $custEmployeeService = FactoryService::getCustEmployeeService();

        $custEmployee = $custEmployeeService->getEmployeeProfileFromUser($user);
        $custEmployee['org_id'] = $organizationId;

        return $custEmployeeService->insert($custEmployee);
    }

    public function linkUserToEmployee($employee, $user)
    {
        $custEmpUserService = FactoryService::getCustEmpUserService();
        $custEmpUser = array();
        $custEmpUser['emp_id'] = $employee['id'];
        $custEmpUser['user_id'] = $user['id'];
        return $custEmpUserService->insert($custEmpUser);
    }

    public function updateUserRoleAdmin($user)
    {
        $user->role_id = Role::ROLE_ADMIN;
        $user->save();
    }
}
