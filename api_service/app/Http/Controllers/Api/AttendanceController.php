<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\FactoryService;
use App\Task;
use App\Helpers\Response;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Facades\JWTAuth;

class AttendanceController extends ApiController
{
    public function __construct()
    {
        $this->service = FactoryService::getAttendanceService();
    }
    
    public function index()
    {
        $attendanceFilter = Input::get();
        $attendanceQueryBuilder = $this->service->getAttendanceListingQueryBuilder($attendanceFilter);

        $attendanceFilter['perPage'] = isset($attendanceFilter['perPage']) ? $attendanceFilter['perPage'] : 10;
        $attendanceFilter['page'] = isset($attendanceFilter['page']) ? $attendanceFilter['page'] : 1;

        $attendancePaginator = $attendanceQueryBuilder->paginate($attendanceFilter['perPage'], ['*'], 'page', $attendanceFilter['page']);

        $pageCount = $attendancePaginator->lastPage();

        $users = $this->service->getAttendanceFromQueryBuilder($attendanceQueryBuilder);

        return Response::responseWithPageCount($users, 200, 'OK', [], $pageCount);
    }

    public function store(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::authenticate($token);

        $attendancesInfo = $request->get('attendances');

        $data = [];
        $errorMsg = [];

        if (empty($attendancesInfo))
        {
            return Response::response($data, 400, 'Data is empty.');
        }
        
        foreach ($attendancesInfo as $info)
        {
            $validator = $this->service->validateInfo($info);
            if ($validator->fails())
            {
                $errors = $validator->errors()->all();
                $errorMsg = array_merge($errorMsg, $errors);
                return Response::responseValidateFailed(implode(' | ', $errorMsg));
            }

           //$task = Task::find($info['task_id']);
            //if (!$task || $task->assigned_to !== $user->id)
            //{
             //   return Response::responseValidateFailed("This task does not belong to you!");
           // }

            $data[] = $this->service->insert($info);
        }

        if (!empty($errorMsg))
        {
            return Response::responseValidateFailed(implode(' | ', $errorMsg), $data);
        }

        return Response::response($data);
    }

    public function show(Request $id)
    {
        $attendance = $this->service->findResource($id);

        if ($attendance)
            return Response::response($this->service->transform($attendance));

        return Response::responseNotFound();
    }

    public function update(Request $request, $id)
    {
        $info = $request->all();
        $lead = $this->service->findResource($id);

        if (!$lead)
            return Response::responseNotFound();

        //validate
        $validator = $this->service->validateInfo($info, 'update', $id);
        if ($validator->fails())
        {
            $errorMsg = $validator->errors()->all();
            return Response::responseValidateFailed(implode(' | ', $errorMsg));
        }
        //update info
        $data = $this->service->update($lead, $info);
        return Response::response($data);
    }

    public function destroy($attendanceId){
        $isAttendancekDeleted = $this->service->delete($attendanceId);

        if ($isAttendancekDeleted)
            return Response::response([]);
        return Response::responseNotFound();
    }


}
