<?php
/**
 * Created by PhpStorm.
 * User: JonnyNguyen
 * Date: 10/08/2016
 * Time: 09:56
 */

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Helpers\Response;
use App\Helpers\LeadHelper;
use App\Http\Requests;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use App\Services\FactoryService;

class StockHeaderController extends ApiController
{
    public function __construct()
    {
        $this->service = FactoryService::getStockHeaderService();
    }

    public function index()
    {
        $stockHeaderFilter = Input::get();
        $stockHeaderQueryBuilder = $this->service->getStockHeaderListingQueryBuilder($stockHeaderFilter);

        $stockHeaderFilter['perPage'] = isset($stockHeaderFilter['perPage']) ? $stockHeaderFilter['perPage'] : 10;
        $stockHeaderFilter['page'] = isset($stockHeaderFilter['page']) ? $stockHeaderFilter['page'] : 1;

        $stockHeaderPaginator = $stockHeaderQueryBuilder->paginate($stockHeaderFilter['perPage'], ['*'], 'page', $stockHeaderFilter['page']);

        $pageCount = $stockHeaderPaginator->lastPage();

        $leads = $this->service->getStockHeaderFromQueryBuilder($stockHeaderQueryBuilder);

        return Response::responseWithPageCount($leads, 200, 'OK', [], $pageCount);
    }

    public function show(Request $request, $stockHeaderId)
    {
        $stockHeader = $this->service->findResource($stockHeaderId);

        if ($stockHeader)
            return Response::response($this->service->transform($stockHeader));

        return Response::responseNotFound();
    }

    public function store(Request $request)
    {
        $stockHeaderInfo = $request->get('stock_headers');

        $data = [];
        $errorMsg = [];

        if (!empty($stockHeaderInfo))
        {
            foreach ($stockHeaderInfo as $info)
            {
                //validate
                $validator = $this->service->validateInfo($info);
                if ($validator->fails())
                {
                    $errors = $validator->errors()->all();
                    $errorMsg = array_merge($errorMsg, $errors);
                } else
                    $data[] = $this->service->insert($info);
            }
        } else
        {
            Response::response($data, 400, 'Data is empty.');
        }

        if (!empty($errorMsg))
        {
            return Response::responseValidateFailed(implode(' | ', $errorMsg), $data);
        }
        return Response::response($data);
    }

    public function update(Request $request, $id)
    {
        $info = $request->all();
        $stockHeader = $this->service->findResource($id);

        if (!$stockHeader)
            return Response::responseNotFound();

        //validate
        $validator = $this->service->validateInfo($info, 'update', $id);
        if ($validator->fails())
        {
            $errorMsg = $validator->errors()->all();
            return Response::responseValidateFailed(implode(' | ', $errorMsg));
        }
        //update info
        $data = $this->service->update($stockHeader, $info);
        return Response::response($data);
    }

    public function destroy($stockHeaderId){
        $isStockHeaderDeleted = $this->service->delete($stockHeaderId);
        if ($isStockHeaderDeleted)
            return Response::response([]);
        return Response::responseNotFound();
    }
}