<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\FactoryService;
use Illuminate\Support\Facades\Input;
use App\Helpers\Response;

class CustomGeocodingController extends ApiController
{
    public function __construct()
    {
        $this->service = FactoryService::getCustomGeocodingService();
    }

    public function index()
    {
        $filter = Input::get();
        $queryBuilder = $this->service->getOpportuneListingQueryBuilder($filter);

        $filter['perPage'] = isset($filter['perPage']) ? $filter['perPage'] : 10;
        $filter['page'] = isset($filter['page']) ? $filter['page'] : 1;

        $paginator = $queryBuilder->paginate($filter['perPage'], ['*'], 'page', $filter['page']);

        $pageCount = $paginator->lastPage();

        $records = $this->service->getOpportunitiesFromQueryBuilder($queryBuilder);

        return Response::responseWithPageCount($records, 200, 'OK', [], $pageCount);
    }

    public function show(Request $request, $id)
    {
        $record = $this->service->findResource($id);

        if ($record)
            return Response::response($this->service->transform($record));

        return Response::responseNotFound();
    }

    public function store(Request $request)
    {
        $newRecords = $request->get('custom_geocodings');

        $data = [];
        $errorMsg = [];

        if (empty($newRecords)) {
            return Response::response($data, 400, 'Data is empty.');
        }

        foreach ($newRecords as $newRecord) {
            $validator = $this->service->validateInfo($newRecord);
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                $errorMsg = array_merge($errorMsg, $errors);
                return Response::responseValidateFailed(implode(' | ', $errorMsg));
            }

            $data = $this->service->insert($newRecord);
        }

        if (!empty($errorMsg)) {
            return Response::responseValidateFailed(implode(' | ', $errorMsg), $data);
        }

        return Response::response($data);
    }

    public function update(Request $request, $id)
    {
        $info = $request->all();
        $record = $this->service->findResource($id);

        if (!$record)
            return Response::responseNotFound();

        //validate
        $validator = $this->service->validateInfo($info, 'update', $id);
        if ($validator->fails())
        {
            $errorMsg = $validator->errors()->all();
            return Response::responseValidateFailed(implode(' | ', $errorMsg));
        }
        //update info
        $data = $this->service->update($record, $info);
        return Response::response($data);
    }

    public function destroy($CustId)
    {
        $isCustDeleted = $this->service->delete($CustId);

        if ($isCustDeleted)
            return Response::response([]);
        return Response::responseNotFound();
    }
}
