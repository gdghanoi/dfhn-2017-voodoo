<?php
/**
 * Created by PhpStorm.
 * User: JonnyNguyen
 * Date: 10/08/2016
 * Time: 09:56
 */

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Helpers\Response;
use App\Helpers\LeadHelper;
use App\Http\Requests;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use App\Services\FactoryService;

class ProductImageController extends ApiController
{
    public function __construct()
    {
        $this->service = FactoryService::getProductImageService();
    }

    public function index()
    {
        $productImageFilter = Input::get();
        $productImageQueryBuilder = $this->service->getProductImageListingQueryBuilder($productImageFilter);

        $productImageFilter['perPage'] = isset($productImageFilter['perPage']) ? $productImageFilter['perPage'] : 10;
        $productImageFilter['page'] = isset($productImageFilter['page']) ? $productImageFilter['page'] : 1;

        $productImagePaginator = $productImageQueryBuilder->paginate($productImageFilter['perPage'], ['*'], 'page', $productImageFilter['page']);

        $pageCount = $productImagePaginator->lastPage();

        $leads = $this->service->getProductFromQueryBuilder($productImageQueryBuilder);

        return Response::responseWithPageCount($leads, 200, 'OK', [], $pageCount);
    }

    public function show(Request $request, $productImageId)
    {
        $productImage = $this->service->findResource($productImageId);

        if ($productImage)
            return Response::response($this->service->transform($productImage));

        return Response::responseNotFound();
    }

    public function store(Request $request)
    {
        $productsImageInfo = $request->get('productsImage');

        $data = [];
        $errorMsg = [];

        if (!empty($productsImageInfo))
        {
            foreach ($productsImageInfo as $info)
            {
                //validate
                $validator = $this->service->validateInfo($info);
                if ($validator->fails())
                {
                    $errors = $validator->errors()->all();
                    $errorMsg = array_merge($errorMsg, $errors);
                } else
                    $data[] = $this->service->insert($info);
            }
        } else
        {
            Response::response($data, 400, 'Data is empty.');
        }

        if (!empty($errorMsg))
        {
            return Response::responseValidateFailed(implode(' | ', $errorMsg), $data);
        }
        return Response::response($data);
    }

    public function update(Request $request, $id)
    {
        $info = $request->all();
        $productImage = $this->service->findResource($id);

        if (!$productImage)
            return Response::responseNotFound();

        //validate
        $validator = $this->service->validateInfo($info, 'update', $id);
        if ($validator->fails())
        {
            $errorMsg = $validator->errors()->all();
            return Response::responseValidateFailed(implode(' | ', $errorMsg));
        }
        //update info
        $data = $this->service->update($productImage, $info);
        return Response::response($data);
    }

    public function destroy($productImageId){
        $isProductImageDeleted = $this->service->delete($productImageId);
        if ($isProductImageDeleted)
            return Response::response([]);
        return Response::responseNotFound();
    }
}