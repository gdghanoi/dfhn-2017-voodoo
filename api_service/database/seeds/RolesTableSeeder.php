<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id' => 0,
                'name' => 'system administrator',
                'description' => ''
            ],
            [
                'id' => 1,
                'name' => 'administrator',
                'description' => ''
            ],
            [
                'id' => 2,
                'name' => 'sale manager',
                'description' => ''
            ],
            [
                'id' => 3,
                'name' => 'salesperson',
                'description' => ''
            ],
        ];

        foreach ($roles as $role)
        {
            Role::create($role);
        }
    }
}
