<?php

use Illuminate\Database\Seeder;

class StockDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\StockDetail::class, 100)->create();
    }
}
