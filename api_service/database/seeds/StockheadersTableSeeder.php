<?php

use Illuminate\Database\Seeder;

class StockHeadersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\StockHeader::class, 100)->create();
    }
}
