<?php

use Illuminate\Database\Seeder;

class CustomGeocodingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\CustomGeocoding::class, 20)->create();
    }
}
