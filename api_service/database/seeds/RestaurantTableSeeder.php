<?php

use Illuminate\Database\Seeder;

class RestaurantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->insert([
            'id' => 1,
            'name' => 'Nha Hang Hai Xom',
            'address' => '12 le thanh nghi, ha noi',
            'location' => ''
        ]);
    }
}
