<?php

use Illuminate\Database\Seeder;

class TeamSaleMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\TeamSalesMember::class, 10)->create();
    }
}
