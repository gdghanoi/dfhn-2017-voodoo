<?php

use Illuminate\Database\Seeder;

class CustEmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cust_employees')->insert([
            'org_id' => 1,
            'prefix' => 'Mr.',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'sex' => 'male',
            'register_email' => 'lakjsdflk@gmail.com',
            'mobile_phone' => '845945827279'
        ]);

        DB::table('cust_employees')->insert([
            'org_id' => 1,
            'prefix' => 'Mr.',
            'first_name' => 'John',
            'last_name' => 'Doe 2',
            'sex' => 'female',
            'register_email' => '2lakjsdflk@gmail.com',
            'mobile_phone' => '2845945827279'
        ]);
    }
}
