<?php

use Illuminate\Database\Seeder;

class BookingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bookings')->insert([
            'id' => 1,
            'restaurant' => '1',
            'table' => '3',
            'number_people' => '12',
            'accept' => '1`',
        ]);
    }
}
