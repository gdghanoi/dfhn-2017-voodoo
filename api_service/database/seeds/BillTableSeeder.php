<?php

use Illuminate\Database\Seeder;

class BillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('res_bills')->insert([
            'id' => 1,
            'menu' => '1',
            'booking' => '1',
        ]);
    }
}
