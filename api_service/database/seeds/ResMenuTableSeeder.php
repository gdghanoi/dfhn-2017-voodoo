<?php

use Illuminate\Database\Seeder;

class ResMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('res_menus')->insert([
            'id' => 1,
            'name' => 'Nha Hang Hai Xom',
            'address' => '12 le thanh nghi, ha noi',
            'location' => ''
        ]);
    }
}
