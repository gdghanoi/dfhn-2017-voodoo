<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOrgIdToLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('leads', function($table)
        {
            $table->integer('org_id')->nullable();
            $table->foreign('org_id')->references('id')->on('cust_organizations')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('leads', function($table)
        {
            $table->dropColumn('org_id');
        });
    }
}
