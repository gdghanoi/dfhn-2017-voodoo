<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('lead_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lead_id');
            $table->integer('task_id');
            $table->integer('remark_id');
            $table->string('rate_level');
            $table->double('amount');
            $table ->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('lead_rates');
    }
}
