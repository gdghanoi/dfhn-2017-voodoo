<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean("primary")->default(false);
            $table->string("zip_code")->nullable();
            $table->string("district")->nullable();
            $table->string("state_province")->nullable();
            $table->string("country")->nullable();
            $table->string("street")->nullable();
            $table->integer("lead_id");
            $table->foreign("lead_id")->references("id")->on("leads")->onDelete('cascade');
            $table->double("lon")->nullable();
            $table->double("lat")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
