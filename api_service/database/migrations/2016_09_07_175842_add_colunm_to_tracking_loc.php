<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColunmToTrackingLoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('tracking_locations', function($table)
        {
            $table->string('source')->nullable();
            $table->string('bearing')->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('tracking_locations', function($table)
        {
            $table->dropColumn('source');
            $table->dropColumn('bearing');

        });
    }
}
