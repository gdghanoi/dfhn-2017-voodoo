var map;
var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png';
function initMap() {
            var uluru = {lat: 21.0586441, lng: 105.8180515};
            map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            mapTypeControl: false,
            center: { lat: 21.0586441, lng: 105.8180515 }
        });
        
            var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
        setMarkers(map);
        marker.addListener('click', function() {
          map.setZoom(8);
          map.setCenter(marker.getPosition());
        });
}

var beaches = [
  ['Bondi Beach', 21.060992,105.8077918, 4],
  ['Coogee Beach', 21.060992,105.81779, 5],
  ['Cronulla Beach', 21.060992,105.8057418, 3],
  ['Manly Beach', 21.060992,105.8076918, 2],
  ['Maroubra Beach', 21.060992,105.8097918, 1]
];
function setMarkers(map) {
  // Adds markers to the map.

  // Marker sizes are expressed as a Size of X,Y where the origin of the image
  // (0,0) is located in the top left of the image.

  // Origins, anchor positions and coordinates of the marker increase in the X
  // direction to the right and in the Y direction down.
  var image = {
    url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
    // This marker is 20 pixels wide by 32 pixels high.
    size: new google.maps.Size(20, 32),
    // The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),
    // The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(0, 32)
  };
  // Shapes define the clickable region of the icon. The type defines an HTML
  // <area> element 'poly' which traces out a polygon as a series of X,Y points.
  // The final coordinate closes the poly by connecting to the first coordinate.
  var shape = {
    coords: [1, 1, 1, 20, 18, 20, 18, 1],
    type: 'poly'
  };
  for (var i = 0; i < beaches.length; i++) {
    var beach = beaches[i];
    var marker = new google.maps.Marker({
      position: {lat: beach[1], lng: beach[2]},
      map: map,
      icon: image,
      shape: shape,
      title: beach[0],
      zIndex: beach[3]
    });
   var infowindow = new google.maps.InfoWindow({
          content: marker.title
        });
        marker.addListener('click', function() {
          infowindow.open(marker.get('map'), marker);
        });
  }
}     
    