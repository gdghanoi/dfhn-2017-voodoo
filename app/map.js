'use strict';

var map;

var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png';

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 21.029246, lng: 105.854217 },
        zoom: 13,
        mapTypeControl: false
    });

    var input = document.getElementById('area');

    var autocomplete = new google.maps.places.Autocomplete(input);
    var controlPanel = document.getElementById('map-control-pane');

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(controlPanel);

    autocomplete.bindTo('bounds', map);
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function () {
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        document.getElementById('input-place-name').innerText = place.name;
    });
}

function searchPlace() {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://cityparkingapi.skymapglobal.vn/api/v1.0/parkings",
        "method": "GET"
    }

    $.ajax(settings).done(function (response) {
        if (!response.parkings || response.parkings.length === 0)
            alert("Lỗi!");
        showResult(response.parkings);
    });
}

function showResult(parkings) {
    clearData();

    parkings.forEach(function (parking) {
        var marker = addMarker(parking);
        var card = addCard(parking);

        function selectParking() {
            highlightCard(card);
            highlightMarker(marker);
        }

        card.click(selectParking);
        marker.addListener('click', selectParking);
    })
}

function clearData() {
    $('#sidebar-content').html("");
    // TODO: Clear marker.
}

function addMarker(parking) {
    var myLatLng = new google.maps.LatLng({ lat: parseFloat(parking.latitude), lng: parseFloat(parking.longitude) });
    var marker = new google.maps.Marker({
        map: map,
        icon: iconBase,
        anchorPoint: new google.maps.Point(0, -29)
    });

    marker.setPosition(myLatLng);
    return marker;
}

function addCard(parking) {
    var card = $("<div class='w3-card w3-bar-item'>\
                    <header class='w3-blue'>\
                        <h5 class='card-header'>"+ parking.name + "</h5\>\
                    </header>\
                    <div class='w3-container card-content'>\
                        <p>Số điện thoại: "+ parking.phone + "</p>\
                        <p>Diện tích: "+ parking.area + "m2</p>\
                        <p>Gửi ôtô: "+ (parking.oto ? "Có" : "Không") + "</p>\
                    </div>\
                </div>");
    $('#sidebar-content').append(card);
    return card;
}

var selectedCard = null;
var selectedMarker = null;

function highlightCard(card) {
    if (selectedCard !== null)
        selectedCard.css('border', 'none');

    card.css('border', 'solid pink');
    selectedCard = card;
}

function highlightMarker(marker) {
    if (selectedMarker !== null)
        selectedMarker.setAnimation(null);

    marker.setAnimation(google.maps.Animation.BOUNCE);
    selectedMarker = marker;
    
    map.panTo(marker.position);
}